# Welcome

Gquery is a minimalistic **JSON query language**.  
Its implementation in python makes easy **JSON manipulation** within shell scripts.

Javascript path-like expressions and special functions can be chained to **extract or test JSON objects**.  
Below a quick example :

_Input JSON_

```
{
  "menu": {
    "id": "file",
    "value": "File",
    "popup": {
      "menuitem": [
        {"value": "New", "id": 0},
        {"value": "Open", "id": 1},
        {"value": "Close", "id": 2}
      ]
    }
  }
}
```
_Queries and output_

```
menu.popup.menuitem[1].value                       # Open
menu.keys()                                        # ["id","value","popup"]
menu.keys().count()                                # 3
menu.popup.menuitem.with_gt("id",1).first().value  # Close
eq(menu.value,"foo")                               # false
eq(menu.value.lower(),"file")                      # true
```

# Getting started

The gquery.py python script applies queries to JSON objects sent to its input stream (sdtin).

### Prototype
```
$ echo 'JSON data' | ./gquery.py 'query'
$ echo 'JSON data' | ./gquery.py 'filtering query' 'rendering query' # see chapter Parameters
```

Exemple :

```
$ echo '{"a":[4,2,9]}' | ./gquery.py 'a.count()'  # 3
```

### Input and output streams

One or multiple JSON data can be sent to the input stream (stdin).  
JSON objects sharing the same input stream must be separated by line feeds.  
Results are sent to the standard output stream (stdout) separated by line feeds.

Example :

```
$ echo '{"a":[4,2,9]}
        {"a":[7]}
        {"a":[5,8]}' | ./gquery.py 'a.count()'
3
1
2
```

### Parameters

When a second parameter is provided,

* the first parameter acts as a filter on the input stream
* the second parameter acts as a renderer

Example :

```
$ echo '{"a":[4,2,9]}
        {"a":[7]}
        {"a":[5,8]}' | ./gquery.py 'ge(a.count(),2)' 'a[1]'
2
8
```


### Error management

Errors are returned 'as this', prefixed with "ERROR: " and printed to the output stream.  
When a single JSON is sent to the input stream, the error code can be:

* 0: no error
* 1: JSON syntax error
* 2: query syntax error
* 3: other errors

**Example: JSON syntax error**

```
$ echo '123abc' | ./gquery.py ''
ERROR: reading json in stdin Extra data: line 1 column 4 - line 2 column 1 (char 3 - 7)
```

**Example: index error**

```
$ echo '{"a":[4,2,9]}
        {"a":[7]}
        {"a":[5,8]}' | ./gquery.py 'a[1]'
2
ERROR: wrong index '1'
8
```

# Operators

Operator . can follow an **object** to access a value from a given **key**, example :

```
user.firstname # value of the key 'firstname' of the object 'user'
```

Operator [] can follow a **list** (or array) to access an element given by its index, example :

```
cities[56]     # 57th element in the list 'cities'
```

# Functions

To extend the possibilites given by the operators, several functions can be used to **filter, parse, transform or group** JSON data.

Below are the list of available functions :

| Function                              | Description |
|---------------------------------------|-------------|
| ```<string>.lower()```                | returns the string in lower case |
| ```<string>.upper()```                | returns the string in upper case |
| ```<list>.count()```                  | returns the number of items in the list |
| ```<list>.group(l,[f,[a]])```         |  |
| ```<list>.sort()```                   | returns the list with sorted elements |
| ```<list>.first()```                  | returns the first element in the list |
| ```<list>.last()```                   | returns the last element in the list |
| ```<list>.index(i)```                 | returns the (i+1)th element in the list |
| ```<list>.indexes(i,j,...)```         | returns a sublist with elements given by indexes |
| ```<list>.values(u,v,..)```           | returns the list of a subset of its elements keys |
| ```<list>.flat(u,v,...)```            | alias of values but formats the output in plain text |
| ```<list>.with_eq(attribute,value)``` | returns a list where elements |
| ```<dict>.keys()```                   | returns the list of keys of the object |
| ```<dict>.keys(a,b,...)```            |  |
| ```eq(a,b)```                         | returns true if a==b else false |
| ```and(c1,c2,c3,...)```               | returns true if all expressions given in parameters are true |
| ```or(c1,c2,c3,...)```                | returns true if at least one expression given in parameters is true |
| ```parse(q1,q2,...)```                | returns a list containing the execution results of the queries given in parameters |

# Known issues

The operator [] may not be chained directly after a function call. Use the alias function _index()_ instead.

_Example_

```
first()[5]        # may not work
first().index(5)  # will always work
```

# Roadmap

* make it a python module without altering current usage

# License

Copyright (C) 2018 Guillaume Adam <galmiza@gmail.com>

This file is free software; as a special exception the author gives unlimited permission to copy and/or distribute it, with or without modifications, as long as this notice is preserved.