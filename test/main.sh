#!/usr/bin/sh

#
# Test run for gquery
# Runs various queries and test observed results match expectations
# Any bug should trigger at least one new input in the file
#

check() {
 STRING=$1
 QUERY_FILTER=$2
 QUERY_RENDER=$3
 EXPECTED=$4
 echo "testing: $STRING $QUERY_FILTER $QUERY_RENDER"
 printf "$STRING" | ./gquery.py "$QUERY_FILTER" "$QUERY_RENDER" > ~/.tmp1
 printf "$EXPECTED" > ~/.tmp2
 diff ~/.tmp1 ~/.tmp2
}

# group
[ 0 -eq 1 ] && (
  check '[{"id":0},{"toto":3},{"id":1},{"id":0},{"id":0}]' 'group("id")' "" '{"0":3,"1":1}'
  check '[{"id":0,"score":9},{"toto":3},{"id":1,"score":4},{"id":0,"score":2},{"id":0}]' 'group("id","min","score")' '' '{"0":2,"1":4}'
  check '[{"id":0,"score":9},{"toto":3},{"id":1,"score":4},{"id":0,"score":2},{"id":0}]' 'group("id","max","score")' '' '{"0":9,"1":4}'
  check '[{"id":0,"score":9},{"toto":3},{"id":1,"score":4},{"id":0,"score":2},{"id":0}]' 'group("id","avg","score")' '' '{"0":5.5,"1":4}'
)

# single line
[ 0 -eq 1 ] && (
  check '{}' '' '' '{}'
  check '"aBcDe"' 'upper()' '' 'ABCDE'
  check '"aBcDe"' 'lower()' '' 'abcde'
  check '[1,2,3]' 'first()' '' '1'
  check '[1,2,3]' 'last()' '' '3'
  check '"001"' 'int()' '' '1'
  check '1' 'string()' '' '1'
  check '"1"' 'string()' '' '1'
  check '[1,2,3]' 'string()' '' '["1","2","3"]'
  check '["1","2","3"]' 'flat()' '' '1 2 3'
  check '[1,2,3]' 'index(0)' '' '1'
  check '[1,2,3]' 'index(1)' '' '2'
  check '[1,2,3]' 'index(2)' '' '3'
  check '[1,2,3]' 'indexes(0,2)' '' '[1,3]'
  check '[1,2,3]' 'count()' '' '3'
  check '[1,3,2]' 'sort()' '' '[1,2,3]'
  check '{"a":1,"b":2,"c":3}' 'keys()' '' '["a","c","b"]'
  check '{"a":1}' 'a' '' 1
  check '{}' 'eq(1,1)' '' '{}'
  check '{}' 'ne(1,1)' '' ''
)

# operators

# boolean
[ 1 -eq 1 ] && (
  check '{"a":1,"b":2}' 'and(eq(a,1),eq(b,2))' 'a' '1'
  check '{"a":1,"b":2}' 'and(eq(a,1),ne(b,2))' 'a' ''
  check '{"a":1,"b":2}' 'and(ne(a,1),eq(b,2))' 'a' ''
  check '{"a":1,"b":2}' 'and(ne(a,1),ne(b,2))' 'a' ''
  
  check '{"a":1,"b":2}' 'or(eq(a,1),eq(b,2))' 'a' '1'
  check '{"a":1,"b":2}' 'or(eq(a,1),ne(b,2))' 'a' '1'
  check '{"a":1,"b":2}' 'or(ne(a,1),eq(b,2))' 'a' '1'
  check '{"a":1,"b":2}' 'or(ne(a,1),ne(b,2))' 'a' ''
)

# patterns filters
[ 0 -eq 1 ] && (
  check '{"a":"toto"}' 'eq(a,"to.*")' 'a' 'toto'
  check '{"a":"toto"}' 'eq(a,"ta.*")' 'a' ''
)

# filters
[ 0 -eq 1 ] && (
  check '{"a":1}' 'eq(a,1)' 'a' '1'
  check '{"a":1}' 'gt(a,1)' 'a' ''
  check '{"a":2}' 'gt(a,1)' 'a' '2'
  check '{"a":{"b":1}}' 'eq(a.b,1)' 'a.b' '1'
  check '{"a":{"b":1}}' 'ne(a.b,1)' 'a.b' ''
  check '{"a":{"b":231}}' 'gt(a.b,1)' 'a.b' '231'
)

# multiline
[ 0 -eq 1 ] && (
  check '{}\n{}\n{}\n' '' '' '{}\n{}\n{}\n'
  check '{"a":1}\n{"a":2}\n{"a":3}\n' 'a' '' '1\n2\n3\n'
  check '{"a":1}\n{"a":2}\n{"a":3}\n' 'eq(1,0)' '' ''
  check '{"a":1}\n{"a":2}\n{"a":3}\n' 'eq(1,1)' 'a' '1\n2\n3\n'
)

# errors
[ 0 -eq 1 ] && (
  check '{"a":1}\n{"b":2}\n{"a":3}\n' 'ne(a,0)' 'a' "1\nERROR: no key 'a' found\n3\n"
  check '{}\n{"a":[]}\n{}\n' 'eq(a.count(),0)' 'a' "ERROR: wrong key 'a'\n[]\nERROR: wrong key 'a'\n"
  check '{}\n{"a":[{"n":0},{"n":1,"x":9},{"n":2}]}\n{}\n' 'eq(a.count(),3)' 'a.with_eq("n",1).first().x' "ERROR: wrong key 'a'\n9\nERROR: wrong key 'a'\n"
  check '{}\n{"a":[{"n":0},{"n":1,"x":9},{"n":2}]}\n{}\n' 'eq(a.with_eq("n",1).first().x,9)' 'a.with_eq("n",1).first()' "ERROR: wrong key 'a'\n{\"n\":1,\"x\":9}\nERROR: wrong key 'a'\n"
  check '{}\n{"a":[{"n":0},{"n":1,"x":9},{"n":2}]}\n{}\n' 'eq(a.with_eq("x",9).first().n,1)' 'a.count()' "ERROR: wrong key 'a'\n3\nERROR: wrong key 'a'\n"
)

rm ~/.tmp1 ~/.tmp2