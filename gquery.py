#!/usr/bin/python
#
# Copyright (C) 2018 Guillaume Adam <galmiza@gmail.com>
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# Last update: 2018/07/11
# Version: 1.0
# 
# Description:
#  Custom query language to parse json
#
# Usage:
#  cat my.json | ./gquery.py <filtering_query> (<rendering_query>)
#
# Error codes:
#  0 ok
#  1 input error
#  2 syntax error
#  3 other error
#
# Examples:
#  cat properties.json | ./gquery.py "body.objects.with_eq("rights","RW").with_eq("hash",false).count()"
#  cat properties.json | ./gquery.py "eq(body.objects[3].keys().sort().count(),4)" "body"
#
# Language reference:
#  <list>[i]                       returns i-th element from the list
#  <list>.count()                  returns number of items in the list
#  <list>.group(l,[f,[a]])         returns aggregated data from the list, where
#                                  l is a ':' separated list of rows, f is the aggregation function among sum,avg,min,max, a the attribute to aggregate
#  parse()                         returns an array containing the parsing result of each given queries
#  <list>.sort()                   returns sorted elements in the list
#  <list>.first()                  returns first elements of the list
#  <list>.last()                   returns last elements of the list
#  <list>.index(i)                 alias of <list>[i]
#  <list>.indexes(i,j,...)         returns a subset of the list
#  <list>.values(u,v,..)           returns a list of objects with a subset of their keys
#  <dict>.keys()                   returns a list of the keys of the object
#  <dict>.keys(a,b,...)            returns a sub list of the object containing the given keys 
#  <list>.flat()                   returns a flat list (space separated) of the values for the given key
#  <dict>.with_eq(attribute,value) returns a subset of a list of objects where attributes match the given condition (eq,ne,lt,le,gt,ge)
#  eq(a,b)                         returns true if a==b (same with ne,lt,le,gt,ge)
#  and(c1,c2,c3,...)               returns true if all conditions are true
#  or(c1,c2,c3,...)                returns true if at least one condition is true
#  <string>.lower()                returns the string with lower case characters
#  <string>.upper()                returns the string with upper case characters

import sys, json, re, collections, copy

# read json(s) from stdin
# =======================
input=[]
for l in sys.stdin:
 input.append(l)
try:

 # try to merge all input lines into one
 data=["".join(input)]
 json.loads(data[0])
except Exception as e:

 # consider one json per line
 data=[]
 for l in input:
  data.append(l)

 
# test type of context
# ====================
def require_type(ctx,t):
 if type(ctx) is not t:
  bye("ERROR: found %s while expecting %s" % (type(ctx),t))

# report error if args count is invalid
# =====================================
def require_args(args,n):
 if len(args)!=n:
  bye("ERROR: %i arguments required, %i given" % (n,len(args)))

# Get closing ) or ]
# ==================
def getClosing(s,q,k):
 np=0 # nb parenthesis
 nb=0 # nb brackets
 while k<len(q) and (q[k]!=s or (np!=0 or nb!=0)):
  if q[k]=="(": np+=1 
  if q[k]=="[": nb+=1 
  if q[k]==")": np-=1 
  if q[k]=="]": nb-=1
  k+=1
 return k
def getClosingParenthesis(q,k):
 return getClosing(")",q,k)
def getClosingBracket(q,k):
 return getClosing("]",q,k)

# extract children
# ================
def getChildren(args):
 args = args+","
 children = []
 i=0
 k=i
 while k<len(args):
  s = args[i:k].strip()
  if args[k]=="," and s.count("(")==s.count(")") and s.count("[")==s.count("]"):
   children.append(args[i:k].strip())
   i=k+1
  k+=1
 for i in range(len(children)): # process children
  c = children[i]
  if len(c)>0 and c[0]=='"' and c[-1]=='"':
   children[i] = c[1:len(c)-1]
  elif c.count(".") or c.count("(") or c.count("[") or c.count(")") or c.count("]"):
   children[i] = parse(jroot,c)
  else:
   try: c = int(c) # try to cast to int
   except:
    if c=="false": children[i]=False # try to cast to bool
    elif c=="true":  children[i]=True
    elif c=="null":  children[i]=None
    else: children[i] = parse(jroot,c)
 return children

# compare two objects
# ===================
# 0:equal, 1:a>b, -1:a<b
def compare(a,b):
 if isinstance(a,(int,long)) or isinstance(b,(int,long)):
  try:
   a = int(a)
   b = int(b)
  except:
   pass #bye("ERROR: invalid integers %s %s" % (str(a),str(b)))
 if  a==b: return 0
 elif a>b: return 1
 else:     return -1

# bye
# ===
def bye(msg):
 print msg
 sys.exit(3)

# parse query
# ===========
def parse(j,q):
 i=0 # index of parser in q
 while i<len(q):
  k=i # tmp index
  while k<len(q) and q[k] not in [".","(","[","]",")"]: k += 1
  key = q[i:k]
  if k==len(q): # nothing after string, assume key to read
   if key!="":
    try:    j = j[key]
    except: bye("ERROR: no key '%s' found" % (key))
   break
  term = q[k]
  i = k+1
  #print "key:", key, "term:", term
  
  if term==".":
   require_type(j,dict)
   try:    j = j[key]
   except: bye("ERROR: wrong key '%s'" % (key))
   
  elif term=="[":
   # search closing "]"
   k = getClosingBracket(q,k+1)
   args = q[i:k].strip()
   i = k+1
   if i<len(q) and q[i]==".": i+=1
   try:    j = j[key][int(args)]
   except: bye("ERROR: wrong index '%s'" % (args))
    
  elif term=="(":
   # search closing ")"
   k = getClosingParenthesis(q,k+1)
   args = q[i:k].strip()
   cs = getChildren(args) if len(args)>0 else []
   i = k+1
   if i<len(q) and q[i]==".": i+=1
   #print "args: ", args
   if key.startswith("with_"):         ## with_xx
    require_type(j,list)
    require_args(cs,2)
    jcp = []
    for o in list(j):
     r = False
     op = key[5:]
     if op=="eq" and cs[0] in o:
      t = type(o[cs[0]])
      if t==unicode: r = (re.match(re.compile("^"+cs[1]+"$"), o[cs[0]])!=None)
      else:          r = (compare(o[cs[0]],cs[1])==0)
     if op=="ne": r = (cs[0] in o and compare(o[cs[0]],cs[1])!=0)
     if op=="ge": r = (cs[0] in o and compare(o[cs[0]],cs[1])>=0)
     if op=="gt": r = (cs[0] in o and compare(o[cs[0]],cs[1])> 0)
     if op=="le": r = (cs[0] in o and compare(o[cs[0]],cs[1])<=0)
     if op=="lt": r = (cs[0] in o and compare(o[cs[0]],cs[1])< 0)
     if r: jcp.append(o)
    j = jcp
   elif key=="index":         ## index
    require_type(j,list)
    try: j = j[int(args)]
    except Exception as e:
     bye("ERROR: invalid index '%s'" % (args))
   elif key=="indexes":       ## indexes
    require_type(j,list)
    jcp = []
    for ix in cs:
     try: jcp.append(j[int(ix)])
     except Exception as e:
      bye("ERROR: invalid index '%s'" % (ix))
    j = jcp
   elif key=="and":           ## and
    b = True
    for c in cs:
     if not c: b = False
    j = b
   elif key=="or":            ## or
    b = False
    for c in cs:
     if c: b = True
     j = b
   elif key=="count" or key=="length":         ## count
    require_type(j,list)
    j = len(j)
   elif key=="group":         ## group
    require_type(j,list)
    if len(cs)==0: bye("ERROR: at least one argument required for function 'group'")
    f = "sum" # aggregation function
    if len(cs)>1: f = cs[1]
    if f!="sum":
     if len(cs)!=3: bye("ERROR: aggregation function %s requires attribute as argument"%(f))
     else: a = cs[2] # attribute
    d = {} # dict to store aggregated data
    cs = cs[0].split(':')
    l = len(cs)
    agg_init = {"sum":0,"avg":[0,0.0],"min":sys.float_info.max,"max":sys.float_info.min}
    for ij in j:
     p = d # pointer in d
     try:
      for ix in range(l):
       c = cs[ix]
       if ij[c] not in p:
        if ix!=l-1: p[ij[c]] = {}
        else: p[ij[c]] = copy.copy(agg_init[f])
       if ix!=l-1: p = p[ij[c]]
       else:
        if f=="sum": p[ij[c]] += 1
        if f=="max": p[ij[c]] = max(p[ij[c]],ij[a])
        if f=="min": p[ij[c]] = min(p[ij[c]],ij[a])
        if f=="avg":
         n = p[ij[c]][1]
         p[ij[c]][0] = (1.0*n*p[ij[c]][0]+ij[a])/(n+1)
         p[ij[c]][1] = n+1
     except: pass
    j = d
   elif key=="first":         ## first
    require_type(j,list)
    j = j[0]
   elif key=="last":          ## last
    require_type(j,list)
    j = j[-1]
   elif key=="int":           ## int
    require_type(j,unicode)
    j = int(j)
   elif key=="keys":          ## keys
    require_type(j,dict)
    if len(cs)==0:
     j = j.keys()
    else:
     jobj = {}
     for c in cs:
      try: jobj[c] = j[c]
      except Exception as e: pass
     j = jobj
   elif key=="parse":          ## parse
    jcp = []
    for c in cs:
     try: jcp.append(parse(j,c.replace("\\\"","\"")))
     except Exception as e: jcp.append(None)
    j = jcp
   elif key=="int":           ## int
    if type(j) is unicode: j = int(j)
   elif key=="string":        ## string
    if type(j) is int: j = str(j)
    if type(j) is list:
     for ix in range(len(j)):
      j[ix] = str(j[ix])
   elif key=="flat":          ## flat
    require_type(j,list)
    s = ""
    if len(cs)==0:
     j = " ".join([str(ij) for ij in j])
    elif len(cs)==1:
     #j = " ".join([str(ij[cs[0]]) for ij in j])
     for ij in j:
      if cs[0] in ij: s += str(ij[cs[0]])+" "
      else:           s += "Undefined "
     j = s
    else:
     for ij in j:
      #s += " ".join([str(ij[ics]) for ics in cs])
      for ics in cs:
        if ics in ij: s += str(ij[ics])+" "
        else:         s += "Undefined "
      s += "\n"
     j = s
   elif key=="sort":          ## sort
    require_type(j,list)
    j = sorted(j)
   elif key=="values":        ## values
    require_type(j,list)
    jcp = []
    for ij in j:
     o = {}
     for c in cs:
      try: o[c] = ij[c]
      except Exception as e: pass
     jcp.append(o)
     j = jcp
   elif key=="eq":            ## eq
    require_args(cs,2)
    u = (type(cs[0])==unicode)
    if u: j = re.match(re.compile("^"+cs[1]+"$"), cs[0])!=None
    else: j = compare(cs[0],cs[1])==0
   elif key=="ne":            ## ne
    require_args(cs,2)
    j = compare(cs[0],cs[1])!=0
   elif key=="lt":            ## lt
    require_args(cs,2)
    j = compare(cs[0],cs[1])<0
   elif key=="le":            ## le
    require_args(cs,2)
    j = compare(cs[0],cs[1])<=0
   elif key=="gt":            ## gt
    require_args(cs,2)
    j = compare(cs[0],cs[1])>0
   elif key=="ge":            ## ge
    require_args(cs,2)
    j = compare(cs[0],cs[1])>=0 
   elif key=="upper":          ## upper
    require_type(j,unicode)
    j = j.upper()
   elif key=="lower":          ## lower
    require_type(j,unicode)
    j = j.lower()
   else:
    bye("ERROR: unsupported function '%s'" % (key))
  else: # can only be an unexpected closing ),],}
   bye("ERROR: syntax")
 return j

# format result
# =============
def print_context(j):
 if type(j) is bool:
  if len(sys.argv)>2: # on optional 2nd parameter, return formatted output only if value is true
   #print "j=",j
   if j: print_context(parse(jroot,sys.argv[2]))
  else:
   sys.stdout.write(json.dumps(j))
   if len(data)>1: sys.stdout.write("\n")
  #if j==False and len(data)==1: exit(3)
 elif type(j) is dict or type(j) is list or j==None:
  if type(j) is dict: j = collections.OrderedDict(sorted(j.items()))
  sys.stdout.write(json.dumps(j, separators=(',',':'), sort_keys=True))
  if len(data)>1: sys.stdout.write("\n")
 else:
  sys.stdout.write(str(j))
  #if j=='false' and len(data)==1: sys.exit(3)
  if len(data)>1: sys.stdout.write("\n")
 
# execution entry point
# =====================

multi = (len(data)>1)

# get 1st parameter
try:
 query = sys.argv[1]
except Exception as e:
 print "ERROR: reading query as parameter", e
 sys.exit(2)
 
# parse all entries
for l in data:
 try:
  jroot = json.loads(l)
 except Exception as e:
  print "ERROR: reading json in stdin", e
  if multi: pass
  else:     sys.exit(1)
 
 try:
  # parse and print
  j = jroot
  j = parse(jroot,query)
  print_context(j)
 except:
  if multi: pass
  else:     sys.exit(3)

